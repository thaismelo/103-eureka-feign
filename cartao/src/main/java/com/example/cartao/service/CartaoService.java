package com.example.cartao.service;

import com.example.cartao.client.ClienteClient;
import com.example.cartao.exception.CartaoAlreadyExistsException;
import com.example.cartao.exception.CartaoNotFoundException;
import com.example.cartao.model.Cartao;
import com.example.cartao.model.Cliente;
import com.example.cartao.repository.CartaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    private CartaoRepository cartaoRepository;

    @Autowired
    private ClienteClient clienteClient;

    public Cartao create(Cartao cartao) {
        Cliente client = clienteClient.getById(cartao.getIdCliente());
        cartao.setIdCliente(client.getId());

        Optional<Cartao> byNumero = cartaoRepository.findByNumero(cartao.getNumero());
        if(byNumero.isPresent()) {
            throw new CartaoAlreadyExistsException();
        }

        cartao.setAtivo(false);

        return cartaoRepository.save(cartao);
    }

    public Cartao update(Cartao updatedCartao) {
        Cartao databaseCartao = getByNumero(updatedCartao.getNumero());

        databaseCartao.setAtivo(updatedCartao.getAtivo());

        return cartaoRepository.save(databaseCartao);
    }

    public Cartao getById(Long id) {
        Optional<Cartao> byId = cartaoRepository.findById(id);

        if(!byId.isPresent()) {
            throw new CartaoNotFoundException();
        }

        return byId.get();
    }

    public Cartao getByNumero(String numero) {
        Optional<Cartao> byId = cartaoRepository.findByNumero(numero);

        if(!byId.isPresent()) {
            throw new CartaoNotFoundException();
        }

        return byId.get();
    }

}
