package com.example.cartao.model.mapper;

import com.example.cartao.model.Cartao;
import com.example.cartao.model.Cliente;
import com.example.cartao.model.dto.create.CreateCartaoRequest;
import com.example.cartao.model.dto.create.CreateCartaoResponse;
import com.example.cartao.model.dto.get.GetCartaoResponse;
import com.example.cartao.model.dto.get.GetIdCartaoResponse;
import com.example.cartao.model.dto.update.UpdateCartaoRequest;
import com.example.cartao.model.dto.update.UpdateCartaoResponse;

public class CartaoMapper {

    public static Cartao fromCreateRequest(CreateCartaoRequest cartaoCreateRequest) {
        Cartao cartao = new Cartao();
        cartao.setNumero(cartaoCreateRequest.getNumero());

        cartao.setIdCliente(cartaoCreateRequest.getClienteId());
        return cartao;
    }

    public static CreateCartaoResponse toCreateResponse(Cartao cartao) {
        CreateCartaoResponse createCartaoResponse = new CreateCartaoResponse();

        createCartaoResponse.setId(cartao.getId());
        createCartaoResponse.setNumero(cartao.getNumero());
        createCartaoResponse.setClienteId(cartao.getIdCliente());
        createCartaoResponse.setAtivo(cartao.getAtivo());

        return createCartaoResponse;
    }

    public static Cartao fromUpdateRequest(UpdateCartaoRequest updateCartaoRequest) {
        Cartao cartao = new Cartao();
        cartao.setAtivo(updateCartaoRequest.getAtivo());
        return cartao;
    }

    public static UpdateCartaoResponse toUpdateResponse(Cartao cartao) {
        UpdateCartaoResponse updateCartaoResponse = new UpdateCartaoResponse();

        updateCartaoResponse.setId(cartao.getId());
        updateCartaoResponse.setNumero(cartao.getNumero());
        updateCartaoResponse.setClienteId(cartao.getIdCliente());
        updateCartaoResponse.setAtivo(cartao.getAtivo());

        return updateCartaoResponse;
    }

    public static GetCartaoResponse toGetResponse(Cartao cartao) {
        GetCartaoResponse getCartaoResponse = new GetCartaoResponse();

        getCartaoResponse.setId(cartao.getId());
        getCartaoResponse.setNumero(cartao.getNumero());
        getCartaoResponse.setClienteId(cartao.getIdCliente());

        return getCartaoResponse;
    }

    public static GetIdCartaoResponse toGetIdResponse(Cartao cartao) {
        GetIdCartaoResponse getIdCartaoResponse = new GetIdCartaoResponse();

        getIdCartaoResponse.setId(cartao.getId());
        getIdCartaoResponse.setAtivo(cartao.getAtivo());

        return getIdCartaoResponse;
    }



}
