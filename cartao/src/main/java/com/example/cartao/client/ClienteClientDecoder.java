package com.example.cartao.client;

import feign.Response;
import feign.codec.ErrorDecoder;

public class ClienteClientDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if (response.status() == 404) {
            throw new RuntimeException("Não encontrado");
        } else {
            return errorDecoder.decode(s, response);
        }
    }
}
