package com.example.cartao.client;

import feign.Feign;
import feign.RetryableException;
import feign.codec.ErrorDecoder;
import io.github.resilience4j.feign.FeignDecorator;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;
import org.springframework.context.annotation.Bean;

public class ClienteClientConfiguration {

    @Bean
    public ErrorDecoder getErrorDecoder() {
        return new ClienteClientDecoder();
    }

    @Bean
    public Feign.Builder builder() {
        FeignDecorators feignDecorators = FeignDecorators.builder()
                .withFallback(new ClienteClientFallback(), RetryableException.class)
                .build();
        //primeiro parametro eu especifico qual  éo fallback,
        //segundo estou dizendo quando o tipo de exception que estou esperando lançar
        return Resilience4jFeign.builder(feignDecorators);
    }
}
