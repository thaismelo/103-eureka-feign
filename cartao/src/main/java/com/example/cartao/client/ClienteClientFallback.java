package com.example.cartao.client;

import com.example.cartao.exception.ClienteOfflineException;
import com.example.cartao.model.Cliente;

public class ClienteClientFallback implements ClienteClient {
    @Override
    public Cliente getById(Long id) {
        throw new ClienteOfflineException();
    }
}
