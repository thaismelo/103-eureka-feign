package com.example.cliente.model.mapper;

import com.example.cliente.model.Cliente;
import com.example.cliente.model.dto.create.CreateClienteRequest;
import org.springframework.stereotype.Component;

@Component
public class ClienteMapper {

    public Cliente toCliente(CreateClienteRequest createClienteRequest) {
        Cliente cliente = new Cliente();
        cliente.setNome(createClienteRequest.getName());
        return cliente;
    }

}
