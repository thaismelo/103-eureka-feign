package com.example.cliente.controller;

import com.example.cliente.model.Cliente;
import com.example.cliente.model.dto.create.CreateClienteRequest;
import com.example.cliente.model.mapper.ClienteMapper;
import com.example.cliente.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @Autowired
    private ClienteMapper mapper;

    @PostMapping("/cliente")
    @ResponseStatus(HttpStatus.CREATED)
    public Cliente create(@RequestBody CreateClienteRequest createClienteRequest) {
        Cliente cliente = mapper.toCliente(createClienteRequest);
        cliente = clienteService.create(cliente);

        return cliente;
    }

    @GetMapping("/cliente/{id}")
    public Optional<Cliente> getById(@PathVariable String id){
        return clienteService.getById(Long.valueOf(id));
    }

}
