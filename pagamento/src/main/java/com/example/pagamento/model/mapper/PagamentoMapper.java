package com.example.pagamento.model.mapper;

import com.example.pagamento.model.Cartao;
import com.example.pagamento.model.Pagamento;
import com.example.pagamento.model.dto.CreatePagamentoRequest;
import com.example.pagamento.model.dto.PagamentoResponse;

import java.util.ArrayList;
import java.util.List;

public class PagamentoMapper {

    public static PagamentoResponse toPagamentoResponse(Pagamento pagamento) {
        PagamentoResponse pagamentoResponse = new PagamentoResponse();

        pagamentoResponse.setId(pagamento.getId());
        pagamentoResponse.setCartaoId(pagamento.getIdCartao());
        pagamentoResponse.setDescricao(pagamento.getDescricao());
        pagamentoResponse.setValor(pagamento.getValor());

        return pagamentoResponse;
    }

    public static List<PagamentoResponse> toPagamentoResponse(List<Pagamento> pagamentos) {
        List<PagamentoResponse> pagamentoResponses = new ArrayList<>();

        for (Pagamento pagamento : pagamentos) {
            pagamentoResponses.add(toPagamentoResponse(pagamento));
        }

        return pagamentoResponses;
    }

    public static Pagamento toPagamento(CreatePagamentoRequest createPagamentoRequest) {
        Pagamento pagamento = new Pagamento();

        pagamento.setDescricao(createPagamentoRequest.getDescricao());
        pagamento.setValor(createPagamentoRequest.getValor());
        pagamento.setIdCartao(createPagamentoRequest.getCartaoId());

        return pagamento;
    }

}
