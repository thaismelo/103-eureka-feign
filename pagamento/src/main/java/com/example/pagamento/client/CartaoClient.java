package com.example.pagamento.client;

import com.example.pagamento.model.Cartao;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name="cartao")
public interface CartaoClient {

    @GetMapping("/cartao/byId/{id}")
    Cartao getById(@PathVariable Long id);
}
