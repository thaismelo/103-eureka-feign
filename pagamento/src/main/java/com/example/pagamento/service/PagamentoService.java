package com.example.pagamento.service;

import com.example.pagamento.client.CartaoClient;
import com.example.pagamento.model.Cartao;
import com.example.pagamento.model.Pagamento;
import com.example.pagamento.repository.PagamentoRepository;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PagamentoService {
    @Autowired
    private PagamentoRepository pagamentoRepository;

    @Autowired
    private CartaoClient cartaoClient;

    public Pagamento criarPagamento(Pagamento pagamento){
        Cartao cartao = cartaoClient.getById(pagamento.getIdCartao());
        if(cartao.getAtivo() == true){
            pagamento.setIdCartao(cartao.getIdCartao());
            return pagamentoRepository.save(pagamento);
        }
        throw new ObjectNotFoundException("", "Cartão não está ativo para pagamentos.");
    }

    public List<Pagamento> buscarPagamentoPorIdCartao(Long cartao_id){
        return pagamentoRepository.findAllByIdCartao(cartao_id);
    }
}
