package com.example.pagamento.controller;

import com.example.pagamento.model.Pagamento;
import com.example.pagamento.model.dto.CreatePagamentoRequest;
import com.example.pagamento.model.dto.PagamentoResponse;
import com.example.pagamento.model.mapper.PagamentoMapper;
import com.example.pagamento.service.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class PagamentoController {

    @Autowired
    private PagamentoService pagamentoService;

    @PostMapping("/pagamento")
    public PagamentoResponse criarPagamento(@RequestBody CreatePagamentoRequest createPagamentoRequest)  {
        Pagamento pagamento = PagamentoMapper.toPagamento(createPagamentoRequest);
        pagamento = pagamentoService.criarPagamento(pagamento);
        return PagamentoMapper.toPagamentoResponse(pagamento);

    }

    @GetMapping("/pagamentos/{cartaoId}")
    public List<PagamentoResponse> exibirPagamentosCartao(@PathVariable Long cartaoId){
        List<Pagamento> pagamentos = pagamentoService.buscarPagamentoPorIdCartao(cartaoId);
        return PagamentoMapper.toPagamentoResponse(pagamentos);
    }
}

